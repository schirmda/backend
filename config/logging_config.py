# pylint: disable=missing-function-docstring
"""
Hier kann die Konfiguration des Loggings festgelegt werden.
"""
import logging


log_level = logging.INFO
# Folgende Log-Level stehen zur Auswahl: INFO, ERROR