# pylint: disable=missing-function-docstring
"""
Hier kann die Konfiguration vom Keycloak festgelegt werden.
"""
import os

keycloak_host = os.environ.get('KEYCLOAK_HOST')
keycloak_realm = os.environ.get('KEYCLOAK_REALM')
