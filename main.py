# pylint: disable=missing-function-docstring
"""
In main finden sämtliche Funktionsaufrufe statt. HTTP-Requests werden hier angenommen.
"""
from flask import Flask, request
from flask_cors import CORS
from assets import status, buildings, storeys, rooms

app = Flask(__name__)
CORS(app)


# Status
@app.route("/api/v2/assets/status", methods=["GET"])
def get_status():
    return status.get_status()


@app.route("/api/v2/assets/health", methods=["GET"])
def get_health():
    return status.get_health()


@app.route("/api/v2/assets/health/live", methods=["GET"])
def get_health_live():
    return status.get_health_live()


@app.route("/api/v2/assets/health/ready", methods=["GET"])
def get_health_ready():
    return status.get_health_ready()


# Buildings
@app.route("/api/v2/assets/buildings", methods=["GET"])
def get_buildings():
    if request.args.get("include_deleted", default="false").lower() == "true":
        return buildings.get_buildings(True)
    else:
        return buildings.get_buildings(False)


@app.route("/api/v2/assets/buildings", methods=["POST"])
def post_buildings():
    data = request.get_json()
    return buildings.post_buildings(data)


@app.route("/api/v2/assets/buildings/<string:id>", methods=["GET"])
def get_building(id):
    return buildings.get_building(id)


@app.route("/api/v2/assets/buildings/<string:id>", methods=["PUT"])
def put_building(id):
    data = request.get_json()
    return buildings.put_building(id, data)


@app.route("/api/v2/assets/buildings/<string:id>", methods=["DELETE"])
def delete_building(id):
    return buildings.delete_building(id)


# storeys
@app.route("/api/v2/assets/storeys", methods=["GET"])
def get_storeys():
    build_id = request.args.get('building_id')
    if build_id is None:
        build_id = " "
    if request.args.get("include_deleted", default="false").lower() == "true":
        return storeys.get_storeys(True, build_id)
    else:
        return storeys.get_storeys(False, build_id)


@app.route("/api/v2/assets/storeys", methods=["POST"])
def post_storeys():
    data = request.get_json()
    return storeys.post_storeys(data)


@app.route("/api/v2/assets/storeys/<string:id>", methods=["GET"])
def get_storey(id):
    return storeys.get_storey(id)


@app.route("/api/v2/assets/storeys/<string:id>", methods=["PUT"])
def put_storey(id):
    data = request.get_json()
    return storeys.put_storey(id, data)


@app.route("/api/v2/assets/storeys/<string:id>", methods=["DELETE"])
def delete_storey(id):
    return storeys.delete_storey(id)


# rooms
@app.route("/api/v2/assets/rooms", methods=["GET"])
def get_rooms():
    storey_id = request.args.get('storey_id')
    if storey_id is None:
        storey_id = " "
    if request.args.get("include_deleted", default="false").lower() == "true":
        return rooms.get_rooms(True, storey_id)
    else:
        return rooms.get_rooms(False, storey_id)


@app.route("/api/v2/assets/rooms", methods=["POST"])
def post_rooms():
    data = request.get_json()
    return rooms.post_rooms(data)


@app.route("/api/v2/assets/rooms/<string:id>", methods=["GET"])
def get_room(id):
    return rooms.get_room(id)


@app.route("/api/v2/assets/rooms/<string:id>", methods=["PUT"])
def put_room(id):
    data = request.get_json()
    return rooms.put_room(id, data)


@app.route("/api/v2/assets/rooms/<string:id>", methods=["DELETE"])
def delete_room(id):
    return rooms.delete_room(id)


if __name__ == "__main__":
    app.run(port=8000, host="localhost")
