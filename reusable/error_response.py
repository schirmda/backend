# pylint: disable=missing-function-docstring
"""
Fehlermeldungen werden hier formatiert.
"""
import uuid


def create_response(message, more_info):
    response = {
        "errors": [
            {
                "code": "bad_request",
                "message": message,
                "more_info": more_info
            }
        ],
        "trace": uuid.uuid4()
    }
    return response
